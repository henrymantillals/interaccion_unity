﻿public class TypeRequest
{
    private static string auth; //agregando variables para la identificacion en switch
    private static string laboratory; //agregando variables para la identificacion en switch

    private static string student; //agregando variables para la identificacion en switch
    private static string teacher; //agregando variables para la identificacion en switch
    private static string practice; //agregando variables para la identificacion en switch
    public TypeRequest()
    {
        auth = "auth";
        laboratory = "laboratory";
        student = "Estudiante";
        teacher = "Docente";
        practice = "practice";
    }
    public string getAuth() //metodo para obtener el valor de la variable
    {
        return auth;
    }
    public string getLaboratory() //metodo para obtener el valor de la variable
    {
        return laboratory;
    }
    public string getPractice() //metodo para obtener el valor de la variable
    {
        return practice;
    }
    public string getStudent() //metodo para obtener el valor de la variable
    {
        return student;
    }
    public string getTeacher() //metodo para obtener el valor de la variable
    {
        return teacher;
    }
    
}
