﻿using System.Collections;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
public class RequestHttp
{
    public static string url= "http://localhost:8000/api/"; //url de dominio
    private static string auth; //url de ruta
    private static string student; //url de ruta
    public static string laboratory; //url de ruta
    private static string practice; //url de ruta
    public RequestHttp()
    {
        auth = url + "login"; //asignando el armado de la url correspondiente
        laboratory = url + "laboratory"; //asignando el armado de la url correspondiente
        practice = url + "practice/create"; //asignando el armado de la url correspondiente
    }
    public string getAuth() //metodo para obtener el valor de la variable
    {
        return auth;
    }
    public string getLaboratory() //metodo para obtener el valor de la variable
    {
        return laboratory;
    }
    public string getPractice() //metodo para obtener el valor de la variable
    {
        return practice;
    }
    public IEnumerator store(string urls, string json, string type) //funciones que recive como parametros la url donde se encuentra el envio de respectivo y el json de los datos
    {
        var request = new UnityWebRequest(urls, "POST"); //creao la peticion post mediante la utilizacion de UnityReqest donde recive la url y el tipo de metodo post
        byte[] bodyRaw = Encoding.UTF8.GetBytes(json); //decodifica el formato json a tipo byte para poder realizar el armado del json que se requiere para el envio a post. 
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw); //cargamos el dato codificado de tipo byte para la trasmicion textualmente como datos del cuerpo de solicitud HTTP esta es un búfer de memoria de código nativo.
        request.downloadHandler = new DownloadHandlerBuffer(); //permite la resepcion de datos de memoria nativa   
        request.SetRequestHeader("Access-Control-Allow-Credentials", "true"); //cors necesarios para los permisos con el servidor y el navegador
        request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST"); //cors necesarios para los permisos con el servidor y el navegador
        request.SetRequestHeader("Content-Type", "*"); //cors necesarios para los permisos con el servidor y el navegador
        request.SetRequestHeader("Content-Type", "application/json"); //se establece el tipo de cabecera que se va utilizar para la cual se establece de tipo json
        yield return request.SendWebRequest(); //realiza el envio de la peticion al servidor para el registro de datos y devuelve la respuesta del mismo en la variable request
        if (request.isNetworkError || request.isHttpError) //verificacion si existio algun problema al enviar los datos dentro del api
        {
            Setting.setting._status = request.responseCode; //tomando el status devuelto por la peticion y asignandole a nuestro objeto de almacenamiento de informacion 
        }
        else
        {
            switch (type) //switch para ir a realizar la activadad dependiendo del tipo
            {
                case "practice":
                    Setting.setting._status = request.responseCode; //tomando el status devuelto por la peticion y asignandole a nuestro objeto de almacenamiento de informacion 
                    break;
            }
        }
    }
    public IEnumerator show(string urls, string type)//funciones que recive como parametros la url donde se encuentra el envio de peticion respectivo.
    {
        UnityWebRequest request = UnityWebRequest.Get(urls); //creao la peticion post mediante la utilizacion de UnityReqest donde recive la url y el tipo de get aqui no es necesario la utilizacion de buffer, debido a que la respuesta se genera apartir del api y no desde el la aplicativo.
        request.SetRequestHeader("Access-Control-Allow-Credentials", "true"); //cors necesarios para los permisos con el servidor y el navegador
        request.SetRequestHeader("Access-Control-Allow-Methods", "GET, POST"); //cors necesarios para los permisos con el servidor y el navegador
        request.SetRequestHeader("Content-Type", "*"); //cors necesarios para los permisos con el servidor y el navegador
        request.SetRequestHeader("Content-Type", "application/json"); //se establece el tipo de cabecera que se va utilizar para la cual se establece de tipo json
        yield return request.SendWebRequest(); //realiza el envio de la peticion al servidor para el registro de datos y devuelve la respuesta del mismo en la variable request
        if (request.isNetworkError || request.isHttpError) //realiza el envio de la peticion al servidor para el registro de datos y devuelve la respuesta del mismo en la variable request
        {
            Setting.setting._status = request.responseCode; //tomando el status devuelto por la peticion y asignandole a nuestro objeto de almacenamiento de informacion 
        }
        else
        {
        }
    }
}
