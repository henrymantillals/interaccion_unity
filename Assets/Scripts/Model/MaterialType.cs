﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MaterialType
{
    private string materialName;
    public string _materialName { set { this.materialName = value; } get { return materialName; } }
    private BreakGlass breakGlassprefabs;
    private List<BreakGlass> breakglass=new List<BreakGlass>();
    private int maxbreak = 1;
    private int index = 0;
    public void Initiate(Transform parent) 
    { 
        for (int i = 0; i < maxbreak; i++) 
        {
            breakglass.Add(EffectBreak.Instantiate<BreakGlass>(breakGlassprefabs, parent)); 
        } 
    }
    public void Impact(Vector3 pos, Vector3 normal)
    {
        if (index >= maxbreak) 
            index = 0;
        breakglass[index].Reubicate(pos, normal); index++;

    }
}
