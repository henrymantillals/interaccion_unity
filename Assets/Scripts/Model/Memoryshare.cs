﻿using System;
using System.Text;
using System.Runtime.InteropServices;
/**
 * Clase para la utilizacion de la memoria compartida para la cual este se encuentra con derechos de autor de la universidad
 */
public class Memoryshare 
{
    private const string ddlPath = "C:/sm/smClient64.dll"; //direccion de la dll para el funcionamiento de la memoria compartida
    [DllImport(ddlPath)]
    static extern int openMemory(String name, int type); //abertura de la memoria compartida para la cual se debe enviar los siguientes parametros name(nombre de la memoria), type(el tipo que pertenece)
    [DllImport(ddlPath)]
    static extern void setInt(String memoria_name, int position, int value); //Escribir en la memoria compartida que fue creada de tipo int
    [DllImport(ddlPath)]
    static extern int getInt(String memoria_name, int position); //Obtiene el valor de la memoria compartida  
    [DllImport(ddlPath)]
    static extern void setFloat(String memName, int position, float value); //Escribir en la memoria compartida que fue creada de tipo float
    [DllImport(ddlPath)]
    static extern float getFloat(String memName, int position); //Obtiene el valor de la memoria compartida
    [DllImport(ddlPath)]
    static extern void setDouble(String memName, int position, double value); //Escribir en la memoria compartida que fue creada de tipo double
    [DllImport(ddlPath)]
    static extern double getDouble(String memName, int position);//Obtiene el valor de la memoria compartida
    [DllImport(ddlPath)]
    static extern void setString(String memName, int position, String str); //Escribir en la memoria compartida que fue creada de tipo string
    [DllImport(ddlPath)]
    static extern Boolean getString(String memName, int position, [MarshalAs(UnmanagedType.LPStr)] StringBuilder str); //Obtiene el valor de la memoria compartida
    [DllImport(ddlPath)]
    static extern int createMemory(String memName, int size, int type); //Crea la memoria compartida.
    [DllImport(ddlPath)]
    static extern void freeMemories();//libera la memoria compartidas
    /**
     * metodo para llamar a la escritura del valor en la memoria compartida 
     */
    public void setInt(string memoria_name,int tipo,int valor, int position)
    {
        setInt(memoria_name, position, valor);
    }
    /**
     * metodo para llamar a la escritura del valor en la memoria compartida 
     */
    public void setfloat(string memoria_name, int tipo, float valor, int position)
    {
        setFloat(memoria_name, position, valor);
    }
    /**
     * metodo para llamar a la escritura del valor en la memoria compartida 
     */
    public void setDouble(string memoria_name, int tipo, double valor, int position)
    {
        setDouble(memoria_name, position, valor);
    }
    /**
     * metodo para llamar a la escritura del valor en la memoria compartida 
     */
    public void setString(string memoria_name, int tipo, string valor, int position)
    {
        setString(memoria_name, position, valor);
    }
    /**
     * metodo para la obtencion del valor en la memoria compartida 
     */
    public int resultInt(String memoria_name, int position)
    {
        int resultado = getInt(memoria_name, position);
        return resultado;
    }
    /**
     * metodo para la obtencion del valor en la memoria compartida 
     */
    public float resultfloat(String memoria_name, int position)
    {
        float resultado = getFloat(memoria_name, position);
        return resultado;
    }
    /**
     * metodo para la obtencion del valor en la memoria compartida 
     */
    public double resultDouble(String memoria_name, int position)
    {
        double resultado = getDouble(memoria_name, position);
        return resultado;
    }
    /**
     * metodo para la obtencion del valor en la memoria compartida 
     */
    public bool resultString(String memoria_name, int position, [MarshalAs(UnmanagedType.LPStr)] StringBuilder str)
    {
        bool resultado = getString(memoria_name, position,str);
        return resultado;
    }
    /**
     * metodo a llamar para la aperturas de la memoria compartida 
     */
    public int openMemoryShare(String memoria_name,int type)
    {
        return openMemory(memoria_name, type);
    }
    /**
     * metodo a llamar para la creacion de la memoria compartida 
     */
    public int createMemoriShare(String memoria_name, int size, int tipo)
    {
        return createMemory(memoria_name, size, tipo);
    }
    /**
     * metodo a llamar para la liberacion de la memoria compartida 
     */
    public void freeMemoriShare()
    {
        freeMemories();
    }

}
