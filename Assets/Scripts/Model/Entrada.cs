﻿using System;
using System.Collections.Generic;
[Serializable] //serializa a la clase para poder hacer los manejos del json
public class Entrada
{
    /**
     * Atributos del modelo entrada
     */
    public string _id;
    public float[] Sp;
    public float[] Error;
    public float[] Accontrol;
    public float[] Out;
    public string id_prac;
}

