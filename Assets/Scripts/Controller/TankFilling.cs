﻿using UnityEngine;
public class TankFilling : MonoBehaviour
{
    [SerializeField] //directivas que permite visualizar el objeto en se muestre en el inspector pero quede privada al momento de hacer la compilacion.
    private GameObject tank;//declracion del objeto que va hacer el recipiente del liquido en el caso de poseer mas de un recipiente se debe colocar de la siguiente manera private GameObject [] tank de esta manera colocamos todos los recipientes en un arreglo y para realizar
    //la activdad dispuesta en los recipientes nos movemos en la posicion del arreglo que contiene el recipiente ejemplo tank[0],tank[1],tank[3] ..... etc
    [SerializeField]
    private Vector3 position; //declaracion de un vector para saber la posicion del recipiente
    private double percent; //porcentaje de llenado que se encuentra el recipiente
    private double heigth; //altura del volumen que esta recibiendo el recipiente
    [SerializeField]
    private double velocity; //es el valor con que velocidad se va realizar la animacion del llenado del recipiente
    [SerializeField]
    private double volume; //volumen del liquido que debe ir en el recipiente calculado;
    private double aux; //creacion de una variable para la ayuda del cambio del signo
    [SerializeField]
    private RunRain runRain = new RunRain();
    [SerializeField]
    private Explotion explotiontank = new Explotion();
    // Start is called before the first frame update
    void Start()
    {
        position = tank.transform.localPosition; //dando la posicion local (cuando los objetos se encuentran dentro de otro po lo peculiar con position recoje la posicion de plano principal mas no la posicion en realidad en la que se encuentra por lo cal simpre que se este trabajando con objetos dentro de otro plano debemos utilizar localposition para que nos devuleva las verdaderas cordenadas que marcan en el transposition) del recipiente en el caso que exista mas se trabaja con arreglo y se le pasa la posicion del mismo ejemplo positicion[0] = tank[1].transform.position esto debende de la acion que se vaya a realizar. 
    }
    // Update is called once per frame
    void Update()
    {
        
        Filling();
    }
    private void Filling()
    {
        volume = Setting.setting._volume; //aqui se cambia por la varibale de la memoria temporal.
        //tank.transform.localPosition = position;
        if (volume != 0) //validando que la funcionalidad solo se ejecute cuando sea diferente al valor de cero
        {
            if (position.x < 0) //validando el signo de la cordenada x  es negativo para poder realizar la transformacion del valor del volumen en negativo para el correcto funcionamiento de la animacion
            {
                aux = -volume; // asignado el valor del volumen a la variable auxiliar para el cambio del signo si cumple la condicion 
            }
            else if (position.x > 0)
            {
                aux = volume; //asignando el valor del volumen a la variable en el caso que no cumpla con la anterior condicion
            }
            if (Setting.setting._volume > Setting.setting._heigh)
            {
                runRain.activerun(true);
            }
            else
            {
                runRain.activerun(false);
            }
            float explotion = System.Convert.ToSingle(Setting.setting._heigh * 1.05f);
            if (Setting.setting._volume >= explotion)
            {
                explotiontank.activeexplotion(true);
            }
            else
            {
                explotiontank.activeexplotion(false);
            }
            percent = (aux * 100) / Setting.setting._heigh; //calculando el porcentaje de llenado con respecto al volumen calculado para vertir en el contenedor
            heigth = (percent * 0.0485f) / 100; //altura que debe tomar el objeto para realizar el llenado
            float scaleY = System.Convert.ToSingle(position.y + heigth * velocity); //calculado el valor
            tank.transform.localScale = new Vector3(tank.transform.localScale.x, scaleY, tank.transform.localScale.z);//colocando el vector de escala para la animacion del agua.
            float transformX = System.Convert.ToSingle(position.x + heigth * velocity);//calculado el valor
            tank.transform.localPosition = new Vector3(transformX, position.y, position.z);//colocando el vector de la posicion.
        }
    }
}
