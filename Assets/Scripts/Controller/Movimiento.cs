﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    private Rigidbody rigidbody;
    [SerializeField]
    private float speed; //velocidad en que se mueve el objeto
    private float deltatime; //toma del tiempo
    private Vector2 vector;
    private Vector3 vector3;
    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //contador= Input.GetAxis("Vertical"); //aumenta el contador en 1
        //Debug.Log(contador); //imprimeindo los valores de los input
        //cubo.transform.Translate(contador, 0, 0); //movimiento dle objeto.
        MovimientoController();
        MoveController();
    }
    //Recolete informacion del objeto
    private void MovimientoController()
    {
        float deltax = Input.GetAxis("Horizontal"); //toma de entrada
        float deltay = Input.GetAxis("Vertical"); //toma de entrada
        vector = new Vector2(deltax, deltay);//alcenamos en un vector las nuevas coordenas
        deltatime = Time.deltaTime; //TOMA EL TIEMpo que ocurre en la escena
    }
    //movea el objeto
    private void MoveController()
    {
        vector3 = rigidbody.velocity; //tomamos la velocidad del componete rigibody
        Vector3 atras = speed * vector.x * deltatime * transform.right; //calculo de las nuevas coordenas 
        Vector3 adelante = speed * vector.y * deltatime * transform.right; //calculo de las nuevas coordenas 
        Vector3 velocidadfinal = atras + adelante; //calculo de la nueva velocidad
        rigidbody.velocity = velocidadfinal; //asignado la nueva velocidad
    }
    private void OnCollisionStay(Collision collision)
    {
        Debug.Log(collision.other.tag);
    }
}
