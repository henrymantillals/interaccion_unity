﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PanelController : MonoBehaviour
{
    [SerializeField]
    private RunOperation insp; //declaramos el objeto del setpoint que va hacer afectado
    [SerializeField]
    private void OnTriggerStay(Collider other) //metodo para detectar si encuentra dentro del collider
    {
        //Debug.Log(other.tag);
        ControlPanel(other.tag);
        alteration(other.tag);
    }
    private void ControlPanel(string tag)
    {
        if (tag.Equals("Player")) //verifica con que objeto esta colicionado mediante la utilizacion de tag
        {
            float controllerUp = Input.GetAxis("InteractUp"); //colocando la configuracion de entrada de dispositivos
            bool activeUp = false, activeDown = false; //variables de activacion para la activacion

            if (controllerUp == 1 ) //verificacion del valor que nos vota la entradas de inputs.
            {
                activeUp = true; //activando la variable
            }
            if(controllerUp == -1)
            {
                activeDown = true;
            }
            if (activeUp) //verificando cual de las entradas esta ejecutando para realizar el cambio del valor del slider
            {
                insp._insp[0].value += 0.01f; //aumento del valor del slider
            }
            if (activeDown)
            {
                insp._insp[0].value -= 0.01f; //disminucion del valor del slider
            }
        }
    }
    private void alteration(string tag)
    {
        if (tag.Equals("Player")) //verifica con que objeto esta colicionado mediante la utilizacion de tag
        {
            float controllerUp = Input.GetAxis("alert"); //colocando la configuracion de entrada de dispositivos
            bool activeUp = false, activeDown = false; //variables de activacion para la activacion
            if (controllerUp == 1)
            {
                activeUp = true; //activando la variable
            }
            if (controllerUp == -1)
            {
                activeDown = true;
            }
            if (activeUp) //verificando cual de las entradas esta ejecutando para realizar el cambio del valor del slider
            {
                insp._insp[1].value += 0.01f; //aumento del valor del slider
            }
            if (activeDown)
            {
                insp._insp[1].value -= 0.01f; //disminucion del valor del slider
            }
        }
    }
}
