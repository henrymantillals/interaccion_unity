﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectBreak : MonoBehaviour
{
    static public EffectBreak effectBreak { get; private set; }
    internal Transform mtransform;
    private List<MaterialType> materialTypes = new List<MaterialType>();
    void Start()
    {
        effectBreak = this;
        mtransform = this.transform;
        foreach (MaterialType m in materialTypes) { 
            m.Initiate(mtransform); 
        }
    }

    public void Impact(string materialName, Vector3 position, Vector3 normal) 
    { 
        foreach (MaterialType m in materialTypes) 
        { 
            if (m._materialName == materialName) 
            { 
                m.Impact(position, normal); 
                break; 
            } 
        } 
    }
}
