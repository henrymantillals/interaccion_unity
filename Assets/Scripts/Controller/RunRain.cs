﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunRain : MonoBehaviour
{
    private ParticleSystem particle; //efecto de particula
    private AudioSource audioSource;
    void Start()
    {
        gameObject.SetActive(false);
        particle = GetComponent<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
    }
    public void activerun(bool active)
    {
        if (active)
        {
            audioSource.Play();
            particle.gameObject.SetActive(active);
            particle.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            audioSource.Stop();
            particle.gameObject.SetActive(false);
            particle.Play(false);
        }
    }
}
