﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakGlass : MonoBehaviour
{
    [SerializeField]
    private AudioClip breakglass; //audio clip
    private ParticleSystem particle; //efecto de particula
    private AudioSource audioSource; //fuente de sonido
    private void Start()
    {
        audioSource = GetComponent<AudioSource>(); //obteniendo el componenete
        particle = GetComponentInChildren<ParticleSystem>(); //obteniendo el componete del objeto hijo
        gameObject.SetActive(false); //desasctivando el objeto

    }
    public void Reubicate(Vector3 position, Vector3 normal)
    {
        audioSource.Stop();//deteniendo el sonido
        gameObject.transform.position = position + normal * 0.01f; //dando la cordenadas del objeto
        gameObject.transform.LookAt(position + normal);
        gameObject.SetActive(true);//activando el objeto
        Debug.Log(Setting.setting._effectScene);
        audioSource.PlayOneShot(breakglass, Setting.setting._effectScene);//realizando el efecto de sonido un corto tiempo
        particle.gameObject.SetActive(true); //activando la particula
        particle.Play(true); //activando la particula
    }
}
