﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model_Math : MonoBehaviour
{
    [SerializeField]
    private RunOperation runOperation = new RunOperation(); //declara la calse de runoperatios ya que el tiene los slider en su clase y reutilizar la clase
    private Memoryshare memoryshare; //instancio la clase de memorysher para realizar la creacion, modificacion y obtencion de datos de la memoria compartida;
    private float k1;         // Constante seccion transversal de la tuberia 1
    private float k2;         // Constante seccion transversal de la tuberia 2
    private float A;          // Area del tanque
    private float g;          // Constante de gravedad
    private float To;         // Tiempo de muestreo
    private float TimeSample; // Referencia del tiempo de muestreo
    private float SP;       // SetPoint real (altura deseada)
    private float a2;       // Apertura real [0-1]
    private float a1;         // Apertura de valvula 1 (CV - Variable controlada)
    private float h_p;        // Proceso - Diferencial de altura del agua en el tanque
    private float h;          // Altura de agua en el tanque
    // Start is called before the first frame update
    private float time;

    void Start()
    {
        memoryshare = new Memoryshare(); //inicializo la clase de memoria compartida (no se realiza lo mismo que en la linea 8 debido a que es una clase intermedia y no es necesario asignarla aun objeto dentro de unity)
        memoryshare.freeMemoriShare(); //libera memmorias
        memoryshare.createMemoriShare("MemoriaONE", 3, 2); //creando la memoria compartida
        memoryshare.createMemoriShare("MemoriaTWO", 3, 2); //creando la memoria compartida
        memoryshare.openMemoryShare("MemoriaONE",2); //abriendo la memoria compartida
        memoryshare.openMemoryShare("MemoriaTWO",2); //abriendo la memoria compartida
        for (int i = 0; i < 3; i++)
        {
            Debug.Log(i);
            memoryshare.setfloat("MemoriaONE", 2, 0, i);
            memoryshare.setfloat("MemoriaTWO", 2, 0, i);
        }
        k1 = 0.05f;
        k2 = 0.015f;
        A = 0.5f; // [m2]
        g = 9.8f; // [m/s2]
        To = 0;   // [s]
        TimeSample = 0.1f; //[s]
    }

    // Update is called once per frame
    void Update()
    {
        modelResult();
    }

    private void modelResult()
    {
        To += Time.deltaTime; // Time.deltaTime = 1/fps, si 60 fps -> Time.deltaTime = 0.01666 [s] = 16.66 [ms] 
        //time += Time.deltaTime;
        
        if (To >= TimeSample)
        {
            
            // SP_n ---------------> DATO QUE TOMO DE LA ESCENA 
            // A2_n ---------------> DATO QUE TOMO DE LA ESCENA 
            time += Time.deltaTime;
            Setting.setting._time = time;
            //Debug.Log(time);
            SP = (runOperation._insp[0].value * System.Convert.ToSingle(Setting.setting._heigh));  // Setpoint - altura deseada real 
            Setting.setting._sp = SP;
            //Debug.Log("sp: " + SP);
            a2 = (runOperation._insp[1].value * System.Convert.ToSingle(Setting.setting._pertuvacionmax)); // Perturvacion - apertura de valvula 2 [0-1]
            Setting.setting._pertuvacion = a2;
            //************ RECIBE DATOS DE MATLAB******************//
            a1 = memoryshare.resultfloat("MemoriaTWO", 0); // Variable controlada - Apertura de V1 para que el error converja a cero
            Setting.setting._a1 = a1;
            //************ MODELO NIVEL **************************
            h_p = (k1 * a1 - k2 * a2 * Mathf.Sqrt(2 * g * h)) / A; // Modelo de la planta ( PROCESO - dh/dt)            
            h = h_p * To + h;  // Altura del tanque (Integracion numerica Metodo de Euler) ---------------> DATO QUE TOMO QUE ENVIO A LA ESCENA 
            Setting.setting._volume = h;
            Setting.setting._error = SP - h;
            //************ ENVIA DATOS A MATLAB******************//
            memoryshare.setfloat("MemoriaONE", 2, SP, 0);
            memoryshare.setfloat("MemoriaONE", 2, a2, 1);
            memoryshare.setfloat("MemoriaONE", 2, h, 2);
            To = 0;
        }
    }
}
