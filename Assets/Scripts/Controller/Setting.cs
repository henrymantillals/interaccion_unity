﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    public static Setting setting; // declaro que publica la clase setting para poder aceder por este medio a los demas atributos y static para que mantenga los datos a llamar a la clase si no colocamos el static
    //al momento de llamar la clase los valor  estaran incialiados en cero o nullos dependiendo el tipo de dato de la varibale que se tenga en este archivo.
    [SerializeField] //directiva que permite ver el objeto en el inspector de unity mas no al momento de realizar la construcion del proyecto.
    private double heigh; //volumen del a soportar el contenedor o recipiente realizado.
    [SerializeField] //directiva que permite ver el objeto en el inspector de unity mas no al momento de realizar la construcion del proyecto.
    private double pertuvacionmax; //volumen del a soportar el contenedor o recipiente realizado.
    [SerializeField]
    private float sp; // valor del setpoint que ser tomado del slider que se construira en la ui.
    [SerializeField]
    private float pertuvacion; // valor del setpoint que ser tomado del slider que se construira en la ui.
    [SerializeField]
    private float volume; //valor del volumen que se obtendra de los calculos realizado.
    [SerializeField]
    private float error; //valor del error que se obtendra de los calculos realizados
    [SerializeField]
    private float effectScene; //valor del effecto que se obtendra 
    [SerializeField]
    private float audioMaster; //valor del effecto que se obtendra 
    [SerializeField]
    private float audioEffects; //valor del effecto que se obtendra
    [SerializeField]
    private long status; //status de la peticion realizada 
    private float timeX;
    private float a1;
    public double _heigh { set { this.heigh = value; } get { return heigh; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public double _pertuvacionmax { set { this.pertuvacionmax = value; } get { return pertuvacionmax; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _sp { set { this.sp = value; } get { return sp; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _pertuvacion { set { this.pertuvacion = value; } get { return pertuvacion; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _volume { set { this.volume = value; } get { return volume; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _error { set { this.error = value; } get { return error; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _effectScene { get { return audioEffects * audioMaster; } }  //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public long _status { set { this.status = value; } get { return status; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _time { set { this.timeX = value; } get { return timeX; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    public float _a1 { set { this.a1 = value; } get { return a1; } } //asignacion de valor privado a una variable publica para la obtencion de los datos utilizando la sintaxis del lenguaje.
    private void Awake() //Esta se ejecuta para inicializar el objeto
    {
        if (Setting.setting == null) //verificar que el objeto se encuentre en nulo
        {
            Setting.setting = this; //se asiga el objeto setting  y this a punta si mismo.
        }
        else
        {
            if (Setting.setting != this) //si setting es diferente de si mismo procede a destruir.
            {
                Destroy(this.gameObject); //Desgtruye el objeto.
            }
        }
        DontDestroyOnLoad(this.gameObject); //crea el objeto pero que no sea destruido al momento de realizar un cambio de escena.
    }
}
