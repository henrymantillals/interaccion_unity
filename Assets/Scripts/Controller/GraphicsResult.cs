﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;

public class GraphicsResult : MonoBehaviour
{
    [SerializeField]
    private GraphChart[] graph;
    public int TotalPoints = 5;
    float lastTime = 0f;
    float lastX = 0f;
    void Start()
    {
        if (graph == null) // the ChartGraph info is obtained via the inspector
            return;
        graph[0].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[0].DataSource.ClearCategory("sp"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[0].DataSource.ClearCategory("h"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[0].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
        graph[1].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[1].DataSource.ClearCategory("a2"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[1].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
        graph[2].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[2].DataSource.ClearCategory("e"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[2].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
        graph[3].DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        graph[3].DataSource.ClearCategory("a1"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        graph[3].DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
    }
    // Update is called once per frame
    void Update()
    {
        float time = Time.time;
        if (lastTime + 0.1f < time)
        {
            lastTime = time;
            graph[0].DataSource.AddPointToCategoryRealtime("sp", Setting.setting._time * 10f, Setting.setting._sp); // each time we call AddPointToCategory 
            graph[0].DataSource.AddPointToCategoryRealtime("h", Setting.setting._time, Setting.setting._volume); // each time we call AddPointToCategory 
            graph[1].DataSource.AddPointToCategoryRealtime("a2", Setting.setting._time, Setting.setting._pertuvacion); // each time we call AddPointToCategory 
            graph[2].DataSource.AddPointToCategoryRealtime("e", Setting.setting._time, Setting.setting._error); // each time we call AddPointToCategory 
            graph[3].DataSource.AddPointToCategoryRealtime("a1", Setting.setting._time, Setting.setting._a1); // each time we call AddPointToCategory 
        }
    }
}
