﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RunOperation : MonoBehaviour
{
    [SerializeField]
    private Slider [] insp; //declaro el objeto de la interfaz grafica
    public Slider[] _insp { set { this.insp=value; }get { return insp; }}//declaro el objeto de la interfaz grafica
    private float sp; //declaro el valor del setpoint
    private float error;//declaro el valor del error
    private float model; //es el valor del modelo calculado a utilizar
    private int tf; //numero de intreacciones que suceden en el tiempo
    private int t; //numero de intreacciones que suceden en el tiempo
    private float volume; //declaro el valor del volumen a calacular
    [SerializeField]
    private TMP_Text[] label; //declaro los labels que tendran los valores de la variables anterior mente declaradas
    // Start is called before the first frame update
    private Memoryshare memoryshare; //Intanciando la clase para la memoria compartida y poder hacer uso de ello
    private int position; //posicion de la memoria compartida
    private float[] Sppoint, Outpoint, errpoint; //valores de los arrreglos de las entradas, saldas, errores votados al realizar
    private RequestHttp requestHttp;
    private TypeRequest typeRequest;
    void Start()
    {
        requestHttp = new RequestHttp();
        typeRequest = new TypeRequest();
        position = 0; //inicializando la posicion 0
        tf = 1; //Inicio del contador
        for(int i = 0; i < label.Length; i++)
        {
            label[i].text = System.Convert.ToString(0); //asignado el valor de cero a los label para la cual utilizamos la transformacion de propo lenguaje.
        }
        memoryshare = new Memoryshare(); //Instancia para la utilizacion de la memoria compartida
        memoryshare.freeMemoriShare();
        memoryshare.createMemoriShare("altura", 1, 2); //creando la memoria compartida
        memoryshare.createMemoriShare("sp", 3, 2); //creando la memoria compartida
        memoryshare.createMemoriShare("error", 1, 2); //creando la memoria compartida
        memoryshare.createMemoriShare("controlador", 1, 2); //creando la memoria compartida
        memoryshare.setfloat("altura", 2, 0, position); //Inicializando los valores de la memoria compartida 
        memoryshare.setfloat("sp", 2, 0, position);//Inicializando los valores de la memoria compartida
        memoryshare.setfloat("sp", 2, 0, 1);//Inicializando los valores de la memoria compartida
        memoryshare.setfloat("sp", 2, 0, 2);//Inicializando los valores de la memoria compartida
        memoryshare.setfloat("error", 2, 0, position);//Inicializando los valores de la memoria compartida
        memoryshare.setfloat("controlador", 2, 0, position);//Inicializando los valores de la memoria compartida
    }
    // Update is called once per frame
    void Update()
    {
        //resultGraphic();
        //heighResult(); //llamamos a la funcion a realizar la tarea.
        label[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._sp, 2)); //asigno el valor a label haciendo que solo apareca dos decimales
        label[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._error, 2));//asigno el valor a label haciendo que solo apareca dos decimales
        label[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._volume, 2));//asigno el valor a label haciendo que solo apareca dos decimales
        label[3].text = System.Convert.ToString(System.Math.Round(Setting.setting._pertuvacion, 2));//asigno el valor a label haciendo que solo apareca dos decimales
    }
    private void heighResult()
    {
        float controller; //declaro el valor del controlador
        sp = System.Convert.ToSingle(insp[0].value * Setting.setting._heigh); //procedo a tomar el valor del slider y transformar a la escala del contendor cabe recalcar que los valos del slider va entre 0 y 1
        label[0].text = System.Convert.ToString(System.Math.Round(sp,2)); //asigno el valor a label haciendo que solo apareca dos decimales
        Setting.setting._sp = sp; //procedo al almacenamiento de la informacion del valor del setpoint en la clase setting
        memoryshare.setfloat("sp", 2, sp, position); //enviando el valor del setpoint en la memoria compartida
        model = (1 - (float)((5307 * Mathf.Exp((float)-(125 * tf) / 193)) / 7720)); //calculando el valor del modelo a utilizar 
        controller = memoryshare.resultfloat("controlador", position); //sacando el valor del controlador que se encuentra en la memoria compartida
        Debug.Log(controller);
        volume = controller * model; //procedo al almacenamiento de la informacion del valor del setpoint en la clase setting
        Setting.setting._volume = volume; //procedo al almacenamiento de la informacion del valor del setpoint en la clase setting
        memoryshare.setfloat("altura", 2, volume, position); //colocando el valor obtendi del volumen en la memoria compartida
        label[2].text = System.Convert.ToString(System.Math.Round(volume,2));//asigno el valor a label haciendo que solo apareca dos decimales
        error = memoryshare.resultfloat("error", 0); //obtencion del valor del valor de la memoria compartida enviada por matlab
        Setting.setting._error = error; //procedo al almacenamiento de la informacion del valor del setpoint en la clase setting
        tf +=  1; //incrementando el contador 
        label[1].text = System.Convert.ToString(System.Math.Round(error,2));//asigno el valor a label haciendo que solo apareca dos decimales
    }
    //private void resultGraphic() // funcion para realizar la grafica
    //{
    //    graphic.AddPoint(sp, 0); //llamando a la asignacion que le pertence en la grafica
    //    graphic.AddPoint(error, 1); //llamando a la asignacion que le pertence en la grafica
    //    graphic.AddPoint(volume, 2); //llamando a la asignacion que le pertence en la grafica
    //}
    public void guardar()
    {
        Entrada entrada = new Entrada(); // Instanciando la clase para la asignacion de los datos
        Sppoint = new float[1]; //asignadole un rango para la creacion del arreglo
        Outpoint = new float[1];//asignadole un rango para la creacion del arreglo
        errpoint = new float[1];//asignadole un rango para la creacion del arreglo
        Sppoint[0] = sp; //asignandole el valor obtendio de la funcion resultadoAltura en el arreglo
        Outpoint[0] = volume; //asignandole el valor obtendio de la funcion resultadoAltura en el arreglo
        errpoint[0] = error; //asignandole el valor obtendio de la funcion resultadoAltura en el arreglo
        entrada.Sp = Sppoint; //asignandole los valores de los arreglos a la clase de entrada
        entrada.Out = Outpoint; //asignandole los valores de los arreglos a la clase de entrada
        entrada.Error = errpoint; //asignandole los valores de los arreglos a la clase de entrada
        entrada.Accontrol = Sppoint; //asignandole los valores de los arreglos a la clase de entrada
        entrada.id_prac = "1"; //codigo de la practica al crear
        string json = JsonUtility.ToJson(entrada); //transformado al formato json para el envio de la api pormedio de request utilizando el protocolo de http 
        StartCoroutine(requestHttp.store(requestHttp.getPractice(),json,typeRequest.getPractice())); //ejecutando la funcion que realiza la peticion request hacia la api.
    }

    private void run()
    {

    }
}
