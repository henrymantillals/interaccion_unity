﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explotion : MonoBehaviour
{
    // Start is called before the first frame update
    private ParticleSystem particle; //efecto de particula
    void Start()
    {
        gameObject.SetActive(false);
        particle = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    public void activeexplotion(bool active)
    {
        if (active)
        {
            particle.gameObject.SetActive(active);
            particle.Play(active);
        }
        else
        {
            gameObject.SetActive(false);
            particle.gameObject.SetActive(false);
            particle.Play(false);
        }
    }
}
